void createFile(void) {

	for( uint8_t i = 0; i < 50; i ++ ) {
      filename[4] = i/10 + '0';
      filename[5] = i%10 + '0';
      if( sd.exists(filename) ) continue;
        break;
    }

   if( !logfile.open( filename, O_CREAT | O_WRITE | O_TRUNC) ) {
    Serial.println( F("SD problem") );
    sd.errorHalt();
   }
}