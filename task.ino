
static void vTaskRadio( void *pvParameters ) {

  // index of record to be filled
  size_t fifoHead = 0;
  // local register for bitwise operations
  byte REG;
  boolean send = false;

  /* Take the semaphore once before starting the loop
  The reason is that at the very beginning the task must be put in sleep
  mode to wait the interrupt routine */
  xSemaphoreTake( interrupt, 0 );

  while(1){
    /* Reread the semaphore, it should be empty. Then wait for a while
    The Interrupt will ripristinate you filling the semaphore */
  	xSemaphoreTake( interrupt, portMAX_DELAY );
  	
    /* Structure to hold all information about the lenght of the pulses 
    from the radio */
    fifoItem *radioPulse = &fifoArray[fifoHead];

    /* Since PORTD is shared with the serial PIND0 and PIND1 the first 2 bits
  	must be masked to be used in the following switch statement */
    REG = PIND_SHADOW & 0b11111100;

    switch( PCMSK2 ){

      case 0x04 : // 0b00000100  ( PCINT18 active!!!)
           if( ((REG & (1<< PIND2)) == 0b00000100) && !(radioPulse->thrFlag) ) {
                radioPulse->thrFlag = 1;
                radioPulse->throttlePulse = cnt;
                break;
              }
           if( ((REG & (1<< PIND2)) == 0b00000000) && (radioPulse->thrFlag) ) {

                radioPulse->throttlePulse = cnt - ( radioPulse->throttlePulse );
                radioPulse->thrFlag = 0;
                /* Change the Interrupt on the next pin */
                PCMSK2 &= ~( 1 << PCINT18 );    // Disable interrupts on this pin
                PCMSK2 |= ( 1 << PCINT19 );     // Enable the interrupts for the next channel
                send = true;
                break;
              }

      case 0x08 : // 0b00001000  ( PCINT19 active!!! )
            if( ((REG & (1<< PIND3)) == 0b00001000) && !(radioPulse->ldgFlag) ) {
                radioPulse->ldgPulse = cnt;
                radioPulse->ldgFlag = 1;
                break;
              }
           if( ((REG & (1<< PIND3)) == 0b00000000) && (radioPulse->ldgFlag) ) {
                radioPulse->ldgPulse = cnt - (radioPulse->ldgPulse);
                radioPulse->ldgFlag = 0;
                /* Change the Interrupt on the next pin */
                PCMSK2 &= ~(1 << PCINT19);    // Disable interrupts on this pin
                PCMSK2 |= (1 << PCINT18);     // Enable the interrupts for the next channel
                break;
              }     

      default:
        break;
    }

  if( send == true ) {
    send = false;
    /* get a buffer */
    if( xSemaphoreTake( fifoSpace, 0 ) != pdTRUE ) {
      Serial.println( F("Overrun error in the FIFO ARRAY") );
    }
    /* signal new data available */
    xSemaphoreGive( fifoData );
    // and advance in the FIFO Array
    fifoHead = fifoHead < (FIFO_SIZE - 1) ? fifoHead + 1 : 0;
   }

  }
}

static void vTaskSendData( void *pvParameters ) {
  // FIFO index to be written
  size_t fifoTail = 0;
  unsigned int count = 0;

	while(1){

    /* Read the semaphore to see if there are new data available , ready to be
    transmitted */
    xSemaphoreTake( fifoData, portMAX_DELAY );
     
    // declare a new structure (pointer) to read data from the FIFO
    fifoItem *radioPulse = &fifoArray[fifoTail];
    
    #if DEBUGLEVEL
    Serial.print( millis() );
    Serial.print( ',' );
    Serial.print( (radioPulse->throttlePulse) );
    Serial.print( ',' );
    Serial.println( (radioPulse->ldgPulse) );
    #endif

    logfile.print( millis() );
    logfile.write( ',' );
    logfile.print( radioPulse->throttlePulse );
    logfile.write( ',' );
    logfile.println( radioPulse->ldgPulse );
    count++;

    if( count == 2048 ){
      count = 0;
      logfile.close();
      #if DEBUGLEVEL
      Serial.println( F("Closed") );
      #endif
      createFile();
    }

    // release record
    xSemaphoreGive( fifoSpace );

    // advance FIFO index
    fifoTail = fifoTail < (FIFO_SIZE - 1) ? fifoTail + 1 : 0;

    }
}
