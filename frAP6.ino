/*******************************************************************************
*
* FreeRTOS for Arduino Nano 3.0
*
* "Copyright (C) 2011 Davide Picchi"  mailto: paveway@gmail.com
* 
* This program is distributed under the terms of the GNU General Public License.
* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
* Autopilot AP6 "Mizard" is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
* (see COPYING file)
*
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*    
*    Davide Picchi
*    
*    Starting date: 03 Juli 2013
*    
*    Last update:   
*
*+++++++++++++++++++++++++++++++++++++++++++++++++
*
*  Diese Version verwendet PORTD, um den Empfänger einzulesen.
* 
*  Einsatz: Razor 6DOF + Arduino Nano 3.0
*
*  Diese Version ist bereits zum fliegen...
*
*************************************************/
#include <avr/interrupt.h>
#include <avr/io.h>

#include <FreeRTOS_AVR.h>
#include <SdFat.h>

#include "define_AP6.h"

/***************************************************************
* Variablen, die durch den ausgeführten Interrupt gelesen werden
***************************************************************/
volatile static unsigned long cnt;
volatile static byte PIND_SHADOW;

/****************************
* Data storage from the Radio
****************************/
struct fifoItem {
  unsigned long throttlePulse;
  unsigned long ldgPulse;
  bool ldgFlag;
  bool thrFlag;
};

// Array for the data incoming from the radio
fifoItem fifoArray[FIFO_SIZE];

/***************************************************************
* Semaphore definitions
***************************************************************/
xSemaphoreHandle interrupt;  // semaphore from the interrupt routine
xSemaphoreHandle fifoData;   // semaphore for counting data storage in the array
xSemaphoreHandle fifoSpace;  // counter of free buffers in FIFO

/***************************
* Output chars and variables
***************************/
void mixLed( void );
void redLed( void );
void greenLed( void );
const int ledRed = 9;    // Nicht 13, da der Pin schon für die SPI eingesetzt ist
const int ledGreen = 8;

/******************************
* Aus dem Datalogger Programm

 The circuit:
 * analog sensors on analog ins 0, 1, and 2
 * SD card attached to SPI bus as follows:
 ** MOSI - pin 11
 ** MISO - pin 12
 ** CLK - pin 13
 ** CS - pin 4

******************************/
// On the Ethernet Shield, CS is pin 4. Note that even if it's not
// used as the CS pin, the hardware CS pin (10 on most Arduino boards,
// 53 on the Mega) must be left as an output or the SD library
// functions will not work.
const uint8_t chipSelect = 10;

SdFat sd;
SdFile logfile;
// create a new file in root, the current working directory
char filename[] = "FLUG00.CSV";

void createFile(void);

//###########################################################################################
static void vTaskRadio( void *pvParameters );
static void vTaskSendData( void *pvParameters );
//###########################################################################################
/**************
*
* Initial Setup
*
**************/
void setup(){
   
   Serial.begin( BAUDRATE );
   Serial.flush();

   delay( 1000 );
   Serial.println( F("********frAP6**********") );
   pinMode( ledGreen, OUTPUT );
   pinMode( chipSelect, OUTPUT );
   delay( 1000 );
   
   // open file
   if( !sd.begin( chipSelect, SPI_HALF_SPEED ) ){
    Serial.println( F("Card initialization failed!") );
    redLed();
   } else {
    Serial.println( F("Card initialized") );
    greenLed();
   }
   
   createFile();

   set_interrupt();

   /* Create the Semaphore and variable for tasks */
   vSemaphoreCreateBinary( interrupt );
   fifoData = xSemaphoreCreateCounting( FIFO_SIZE, 0 );
   fifoSpace = xSemaphoreCreateCounting( FIFO_SIZE, FIFO_SIZE );

   portBASE_TYPE s1, s2;

   /* Check if the semaphore has been created */
   if( interrupt == NULL || fifoData == NULL || fifoSpace == NULL ) {

    Serial.println( F("Cannot create the semaphore") );
   }

   s1 = xTaskCreate( vTaskRadio, 
                     (signed char *)"T1", 
                     configMINIMAL_STACK_SIZE + 20, 
                     NULL, 
                     2, 
                     NULL );
   s2 = xTaskCreate( vTaskSendData, 
                     (signed char *)"T2", 
                     configMINIMAL_STACK_SIZE + 160, 
                     NULL, 
                     1, 
                     NULL );

   /* Check Tasks creations */
   if( s1 != pdPASS || s2 != pdPASS ) {
    Serial.println( F("Creation problem") );
    while(1);
   }

   Serial.print( F("Tasks created...") );
   Serial.println( F("OK") );

   /* Start activities... */
   vTaskStartScheduler();
   Serial.println( F("Insufficient RAM") );
   while(1);

}


void loop(){
  /* Nothing here */
}

/*****************************
*
*     Interrupt Routine
*
*****************************/
void set_interrupt(void){
  
   EICRA = 0x00;
   
   DDRD &= ~((1 << PIND2));   // Input Pin - Es werden nur die ersten D2 und D3 als Interrupt verwendet 
   PORTB &= ~((1 << PIND2));  // Pull - up disabled
   
   // Configure external interrupts on PCINT2 (from PD0 to PD7)
   PCMSK2 = 0;                 // Disable interrupts on over a single port
   PCICR |= (1 << PCIE2);      // Enabled interrupts on PCINT23-16
   PCMSK2 |= ((1 << PCINT18)); // At the moment only PD2 enabled

}