#ifndef define_h
#define define_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#define DEBUGLEVEL 0
#define RADIO_TYPE 0  // 0 => Die Empfängers Outputs sind parallel zueinander: zB: Futaba
                      // 1 => Die Empfängers Outputs überlappen sich nicht, sind nacheinander: zB: 2.4 GHz
#define ESC_ARM 0     // 0 Der Regler wird nicht gearmt
                      // 1 Der Regler wird gearmt

#define BAUDRATE 115200

#define FIFO_SIZE 20  // size of FIFO array

// Diese beiden Werte wurden einfach vorher gemessen und dienen zur Berechnung des Motors. Bei einer anderen Anlage, bitte ändern
#define lengthThrottleMin 950
#define lengthThrottleMax 1850

// Hiermit sind die "Grenzen" für den LDG-Kanal definiert
#define minSignalLength 1000  // Unter dieser Grenze wird das Schreiben der Daten gestoppt
#define maxSignalLength 1800 // Über dieser Grenze wird den Autopilot eingeschaltet
// Für die Radio
#define NUM_CHANNEL 2
#define CH_THR 0
#define CH_LDG 1

#endif
