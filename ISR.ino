#if RADIO_TYPE == 0

ISR(PCINT2_vect){
  
  cnt = micros();
  PIND_SHADOW = PIND;
  
  /* Give the semaphore to unblock the task */
  xSemaphoreGiveFromISR( interrupt, NULL );

}

#endif